$(".go-top").click(function () {
	$('html, body').stop().animate({
		scrollTop: 0
	}, 1000);
});

/*------ Обратная связь ----------*/
$(".button-callback").click(function(){
	$("#block_callback").show();
});
$("#close-callback").click(function(){
	$("#block_callback").hide();
});
$("#ok-button").click(function(){
	if(!$("#phone-client").val()) {
		alert("Вы не оставили свой контакт! Пожалуйста, заполните это поле.");
		return;
	}
	
	if(!$("#name-client").val()) {
		alert("Напишите, пожалуйста, как к Вам обращаться");
		return;
	}
	
	$.ajax({
	  type: "POST",
	  url: "send_callback.php?action=send_message",
	  data: "name="+ $("#name-client").val() +"&phone="+ $("#phone-client").val(),
	  success: function(data){ 
		if(data) { 
			$(".callback-form").addClass("novisible");
			$(".say-ok").show("slow").fadeOut(8000);
		} else {
			alert("Ошибка! Увы, сервер не доступен..");
		}
	  }
	});
});

// Получить бесплатный пробник
$(".give-me-my-share").click(function(){
	$(".callback-form").removeClass("novisible");	
});
$(".close-callback-form").click(function(){
	$(".callback-form").addClass("novisible");	
});

// Получить бесплатный пробник на внутренних страницах
$(".give-me-my-share, .give-me-my-share-yel").click(function(){
	$(".callback-form").removeClass("novisible");	
});
$(".close-callback-form").click(function(){
	$(".callback-form").addClass("novisible");	
});
