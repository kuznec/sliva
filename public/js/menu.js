// Переход на страницу отзывов
$("#review-elem-menu").click(function () {
    window.location.href = "reviews.html";
});
$("#order-elem-menu").click(function () {
    $('html, body').stop().animate({scrollTop: $('#order-block').offset().top}, 1000);
    switchCur(this);
});
$("#product-elem-menu").click(function () {
    $('html, body').stop().animate({scrollTop: $('#product-block').offset().top}, 1000);
    switchCur(this);
});
$("#video-elem-menu").click(function () {
    $('html, body').stop().animate({scrollTop: $('#video-block').offset().top}, 1000);
    switchCur(this);
});
$("#contact-elem-menu").click(function () {
    $('html, body').stop().animate({scrollTop: $('#contact-block').offset().top}, 1000);
    switchCur(this);
});

$(document).ready(function () {
    // Признак главной страницы
    if (!$("#order-block").attr("id")) {
        return;
    }

    widthScreen = document.body.clientWidth;

    if (widthScreen < 700) {
        $(window).scroll(function () {
            var _pos = window.pageYOffset;
            if (_pos < 1200) {
                switchCur("#order-elem-menu");
            }
            if (_pos >= 1200 && _pos < 2200) {
                switchCur("#video-elem-menu");
            }
            if (_pos >= 2200 && _pos < 3200) {
                switchCur("#product-elem-menu");
            }
            if (_pos >= 3200) {
                switchCur("#contact-elem-menu");
            }
        });
        return;
    }
    $(window).scroll(function () {
        var _pos = window.pageYOffset;
        if (_pos < 600) {
            switchCur("#order-elem-menu");
        }
        if (_pos >= 600 && _pos < 1100) {
            switchCur("#video-elem-menu");
        }
        if (_pos >= 1100 && _pos < 1500) {
            switchCur("#product-elem-menu");
        }
        if (_pos >= 1500) {
            switchCur("#contact-elem-menu");
        }
    });
});

switchCur = function (elem) {
    deleteCur();
    setCur(elem);
}

deleteCur = function () {
    $(".elem").removeClass("cur");
    $(".elem").children("a").removeClass("cur");
}

setCur = function (elem) {
    $(elem).addClass("cur");
    $(elem).children("a").addClass("cur");
}
