$(document).ready(function() {	
	var objFieldCount = $("#count-plums");
	var objFieldPrice = $("#all-price");
	var basePrice = 1500;
	var minCountPlums = 1;
	var maxCountPlums = 999;
	
	objFieldCount.keyup(function() {
		checkNumField();
		refreshHowMuchAll();
	});
	
	$("#minus-num-plums").click(function(){
		minusCountPlums();
	});

	$("#plus-num-plums").click(function(){
		plusCountPlums();
	});
	
	refreshHowMuchAll = function() {
		var count = parseInt(objFieldCount.val());
		objFieldPrice.html((basePrice*count).toString());
	}

	minusCountPlums = function() {
		var countPlums = parseInt(objFieldCount.val());
		var newCountPlums = countPlums - 1;
		if (newCountPlums < minCountPlums) {
			newCountPlums = countPlums;
		}
		objFieldCount.val(newCountPlums.toString());
		refreshHowMuchAll();
	}

	plusCountPlums = function() {
		var countPlums = parseInt(objFieldCount.val());
		var newCountPlums = countPlums + 1;
		if (newCountPlums > maxCountPlums) {
			newCountPlums = countPlums;
		}
		objFieldCount.val(newCountPlums.toString());
		refreshHowMuchAll();
	}
	
	checkNumField = function() {
		objFieldCount.val(objFieldCount.val().replace(/\D/g,''));
	}
});