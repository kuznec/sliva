/*
---
name: Locale.ru-RU.uploadManager
description: Russian Language File for uploadManager
authors: Thierry Bela
requires: [More/Locale]
provides: Locale.ru-RU.uploadManager
...
*/

Locale.define('ru-RU', 'uploadManager', {
    BROWSE: '�����',
    CANCEL: '������',
	DROP_FILE_HERE: '��� �������� ����������� ���� �����',
	EMPTY_FILE: '��������� ���� ���� ����',
	FILE_CORRUPTED: '���� ���������',
    MAX_FILE_SIZE_EXCEEDED: function (size) {
	
		return '������� ������� ���� (������ ������ ���� �� ����� ' + size + ')'
	},
	PAUSE: '�����',
	PREFETCH_FAILED: '��������� �������� ���������� � �����',
	RESUME: '����������',
	RETRY: '���������',
    TOTAL_FILES_SIZE_EXCEEDED: function (size) {
	
		return '����� ������ ������ ������ ���� �� ����� ' + size + ''
	},
	UPLOADING: '��������, ���������� ��������� ...',
    UNAUTHORIZED_FILE_TYPE: '�������� ������ �����'
});
