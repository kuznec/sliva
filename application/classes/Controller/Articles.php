<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Articles extends Controller_Common
{
    public function action_index()
    {
        $page = Request::current()->param('page');
        $articles = Model::factory('Articles')->get_all();
        $this->template->name = 'Статьи';
        $this->template->title = "Статьи - ".$this->template->title;
        $this->template->content = View::factory("articles/all")->set('articles',$articles);
    }
    
    public function action_item()
    {
        $url = Request::current()->param('page');
        $article = Model::factory('Articles')->get_article($url);
        if($article->count() == 0)
        {
            throw new HTTP_Exception_404('Страница не найдена'); // Тададам!
            exit();
        }
        $this->template->name = $article[0]->name;
        $this->template->title = "Статьи: ".$article[0]->name." - ".$this->template->title;
        $imgs = Model::factory('Articles')->get_img($article[0]->id);
        $this->template->content = View::factory("articles/item")
                ->set('img', $imgs)
                ->set('id', $article[0]->id)
                ->set('name', $article[0]->name)
                ->set('text', $article[0]->text)
                ->set('date', $article[0]->date);
    }
} // End Articles