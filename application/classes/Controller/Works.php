<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Works extends Controller_Common
{

    public function action_index()
    {
        if($this->request->param('page'))
            $this->page_work($this->request->param('page'));
        else
        {
            $works = Model::factory('Works')->get_all();
            //$last = Model::factory('Works')->get_last();
            $name = Model::factory("Pages")->get_page_by_translit(strtolower($this->request->controller()));
            $this->template->name = $name[0]->name_menu;
            $this->template->title = $name[0]->name_menu." - ".$this->template->title;
            $this->template->content = View::factory('all_works')
                    ->set('works', $works)
                    ->bind('last', $last);
        }
    }
    
    private function page_work($name)
    {
        $works = Model::factory('Works')->get_work($name);
        if(!$works[0])
        {
            throw new HTTP_Exception_404('Страница не найдена'); // Тададам!
            exit();
        }
        else
        {
            $this->template->name = $works[0]->name;
            $this->template->title = $works[0]->name." - ".$this->template->title;
            $this->template->content = View::factory('page_work')->set('translit', $name)->set('text', $works[0]->text);
        }
    }

} // End Works