<?php defined('SYSPATH') or die('No direct script access.');
 
class Controller_Admin_Blocks extends Controller_Admin_Security
{        
    public function action_index()
    {
        $controller = strtolower($this->request->controller());
        $blocks = Model::factory('Admin_Blocks')->get_by_time(date('H:i:s'));
        $html = View::factory('admin/blocks/'.$blocks[0]->id);
        $this->template->content = $html;
        $name = $this->name_module($controller);
        $this->template->action = View::factory('admin/slides/action');
        if($name !== FALSE)
            $this->template->name = $name['name'];
    }
}