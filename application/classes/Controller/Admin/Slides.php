<?php defined('SYSPATH') or die('No direct script access.');
 
class Controller_Admin_Slides extends Controller_Admin_Security
{        
    public function action_index()
    {
        $controller = strtolower($this->request->controller());
        $slides = Model::factory('Admin_Slides')->get_all();
        $html = View::factory('admin/slides/all')
                    ->set('slides', $slides)
                    ->set('controller', $controller);
        $this->template->content = $html;
        $name = $this->name_module($controller);
        $this->template->action = View::factory('admin/slides/action');
        if($name !== FALSE)
            $this->template->name = $name['name'];
    }
    
    public function action_edit()
    {
        $id = $this->request->param('id');
        if($id)
        {
            if($this->request->post())
                $this->edit($id);
            $slide = Model::factory('Admin_Slides')->get_slide($id);
            $html = View::factory('admin/slides/edit')->set('slide', $slide);
            $this->template->content = $html;
            $this->template->action = View::factory('admin/slides/action');
            $this->template->name = "Редактирование слайда";
        }
    }
    
    private function edit($id)
    {
        $h1 = $this->request->post('h1');
        $text1 = $this->request->post('text1');
        $cost1 = $this->request->post('cost1');
        $h2 = $this->request->post('h2');
        $text2 = $this->request->post('text2');
        $h3 = $this->request->post('h3');
        $text3 = $this->request->post('text3');
        $start = false;
        if($this->request->post('start'))
            $start = date('H:i:s', strtotime($this->request->post('start')));
        $stop = false;
        if($this->request->post('stop'))
            $stop = date('H:i:s', strtotime($this->request->post('stop')));
        Model::factory('Admin_Slides')->set_slide($id,$h1,$text1,$cost1,$h2,$text2,$h3,$text3,$start,$stop);
    }
}