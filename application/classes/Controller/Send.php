<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Send extends Controller_Common
{
    public $template = 'send';

    public function action_index()
    {
        $type = 0;
        if(isset($_POST['probs']))
            $type = "1";
        elseif(isset($_POST['zakaz']))
            $type = "2";
        
        if($type != 0)
        {
            $setting = Model::factory('Admin_Settings')->get_all();
            $this->template->phone = $setting[0]->phone;
            
            if(isset($_POST['phone']))
                $phone = $_POST['phone'];
            if(isset($_POST['name']))
                $name = $_POST['name'];
            if(isset($_POST['count']))
                $count = $_POST['count'];


            $post = Validation::factory($_POST); // готовимся к проведению валидации
            $post->rule('phone', 'not_empty');
            $post->rule('phone', 'min_length', array(':value', 6));
            $post->rule('phone', 'max_length', array(':value', 17));
            if($post -> check())
            {
                /* Сюда впишите свою эл. почту */
                $address = "rommm108@gmail.com, vrubel777@yandex.ru, yasv-777@yandex.ru";

                /* А здесь прописывается текст сообщения, \n - перенос строки */
                if($type == 1)
                {
                    $sub = 'Просят пробник!';
                    $mes = "Тема: Просят пробник!<br>Телефон: $phone<br>Имя: $name<br>";
                }
                else
                {
                    $sub = 'Заказали '.$count.' коробок сливы!';
                    $mes = "Тема: Заказали $count коробок сливы!<br>Телефон: $phone<br>Имя: $name<br>";
                }
                /* А эта функция как раз занимается отправкой письма на указанный вами email */
                $headers = "Content-Type: text/html; charset=utf-8\n";  
                $headers .= "MIME-Version: 1.0\n";  
                $headers .= "Content-Transfer-Encoding: 8bit\n";
                $headers .= "From: shareplams@yandex.ru";
                
                
                mail($address, $sub, $mes, $headers);
                ini_set('short_open_tag', 'On');
                header('Refresh: 3; URL=/');
            }
            else
            {
                $this->template->error = "Неверно заполнены или незаполнены поля телефон или E-mail";
            }
        }
    }

}