<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Welcome extends Controller_Common {
    
    public function action_how()
    {
        $this->template->set_filename('how');
    }
    
    public function action_reviews()
    {
        $this->template->set_filename('reviews');
    }

} // End Welcome