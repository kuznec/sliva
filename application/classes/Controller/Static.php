<?php defined('SYSPATH') or die('No direct script access.');

class Controller_Static extends Controller_Common
{
    public $template = 'main';

    public function action_index()
    {
        $page = Request::current()->param('page');
        $info = Model::factory('Pages')->get_page_by_translit($page);
        if($info->count() == 1)
        {
            foreach ($info as $inf)
            {
                $name = $inf->name_menu;
                $title = $inf->title;
                $description = $inf->description;
                $keywords = $inf->keywords;
                $tizer = $inf->tizer;
                $text = $inf->text;
                $seo = $inf->seo;
            }

            $this->template->name = $name;
            if(empty($title))
                $title = $name;
            $this->template->title = $title;
            $this->template->keywords = $keywords;
            $this->template->description = $description;
            
            /*$blocks = Model::factory('Admin_Blocks')->get_by_time(date('H:i:s'));
            if($page == '' && $blocks->count() > 0)
            {
                $this->template->content = View::factory('blocks/'.$blocks[0]->id);
            }*/
            if($page == '')
            {
                //Подгружаем текст для слайдов с бд
                $slides = Model::factory('Admin_Slides')->get_all();
                
                //Вызываем вьюху блока контактов
                $contacts = View::factory('contacts')
                        ->set('email', $this->_settings['email'])
                        ->set('phone', $this->_settings['phone'])
                        ->set('address', $this->_settings['text_bottom']);
                
                $flag1 = true;
                if($slides[0]->start && $slides[0]->stop)
                    $flag1 = false;
                    if( (date('H:i:s') > $slides[0]->start) && (date('H:i:s') < $slides[0]->stop) )
                        $flag1 = true;
                if($flag1)
                {
                    //Вызываем вьюху 1 слайда
                    $slide1 = View::factory('slides/slide1')
                            ->set('info', $slides[0]);
                }
                
                $flag2 = true;
                if($slides[1]->start && $slides[1]->stop)
                    $flag2 = false;
                    if( (date('H:i:s') > $slides[1]->start) && (date('H:i:s') < $slides[1]->stop) )
                        $flag2 = true;
                if($flag2)
                {
                    //Вызываем вьюху 2 слайда
                    $slide2 = View::factory('slides/slide2')
                        ->set('info', $slides[1]);
                }
                
                $flag3 = true;
                if($slides[2]->start && $slides[2]->stop)
                    $flag3 = false;
                    if( (date('H:i:s') > $slides[2]->start) && (date('H:i:s') < $slides[2]->stop) )
                        $flag3 = true;
                if($flag3)
                {
                    //Вызываем вьюху 3 слайда
                    $slide3 = View::factory('slides/slide3')
                            ->set('info', $slides[2]);
                }
                //Вызываем вьюху футера
                $footer = View::factory('footer');
                
                //Передаём все вьюхи в переменную шаблону
                $this->template->content = View::factory('content')
                        ->set('contacts', $contacts)
                        ->bind('slide1', $slide1)
                        ->bind('slide2', $slide2)
                        ->bind('slide3', $slide3)
                        ->set('footer', $footer);
            }
            else
            {
                $this->template->content = View::factory('static')
                        ->set('name', $name)
                        ->set('text', $text);
            }
        }
    }

} // End Static