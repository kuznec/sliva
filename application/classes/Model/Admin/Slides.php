<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Admin_Slides extends Model
{
    protected $_table = "slides";
    
    public function get_slide($id)
    {
        return DB::select()
                    ->from($this->_table)
                    ->where('id','=',$id)
                    ->as_object()
                    ->execute();
    }
    
    public function set_slide($id, $h1, $text1, $cost1, $h2, $text2, $h3, $text3, $start, $stop)
    {
        DB::update($this->_table)
                ->set(array('h1' => $h1, 'text1' => $text1, 'cost1' => $cost1, 'h2' => $h2, 'text2' => $text2, 'h3' => $h3, 'text3' => $text3, 'start' => $start, 'stop' => $stop))
                ->where('id', '=', $id)
                ->execute();
    }
    
    public function get_all()
    {
        return DB::select()
                    ->from($this->_table)
                    ->as_object()
                    ->execute();
    }
}