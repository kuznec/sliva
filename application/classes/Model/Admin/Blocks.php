<?php defined('SYSPATH') or die('No direct script access.');
 
class Model_Admin_Blocks extends Model
{
    public function get_by_time($time)
    {
        return DB::select()
                ->from('blocks')
                ->where('status','=',1)
                ->where('start','<',$time)
                ->where('stop','>',$time)
                ->as_object()
                ->execute();
    }
}