<section class="post">
    <div class="container">
        <h1><?= $name; ?></h1>
        <div class="row">
            <article class="article"><?php
                foreach($img as $k => $v)
                { ?>
                    <div class="art_img">
                        <img src="<?= URL::base('http'); ?>public/articles/<?= $id; ?>/<?= $v->photo; ?>" >
                    </div><?php
                } ?>
                <div class="bl">
                    <p><?= $text; ?></p>
                </div>
                <div class="bl">
                    <dd><?= date('d-m-Y', strtotime($date)); ?></dd>
                </div>
            </article>
        </div>
    </div>
</section>