<section class="post">
    <div class="container">
        <h1>Статьи</h1>
        <div class="row"><?php
            foreach($articles as $k => $v)
            { ?>
            <article class="article">
                <div class="row">
                    <div class="col-sm-12 col-md-6 col-lg-4 img-holder">
                        <img src="<?= URL::base('http'); ?>public/articles/<?= $v->id; ?>/<?= $v->photo; ?>" alt="<?= $v->name; ?>">
                    </div>

                    <div class="col-sm-12 col-md-6 col-lg-8 post-cnt">
                        <h2><?= $v->name; ?></h2>
                        <p><?= $v->caption; ?></p>
                        <a href="<?= URL::base('http'); ?>articles/item/<?= $v->translit; ?>.html">Подробнее...</a>
                    </div>
                </div>
            </article><?php
            } ?>
        </div>
    </div>
</section>