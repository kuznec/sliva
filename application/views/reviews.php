<html>
    <head>
        <title>Отзывы о продуктах SHARE PLUMS</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?= URL::base(); ?>public/i/ico.ico" />
        <link rel="stylesheet" type="text/css" href="<?= URL::base(); ?>public/css/style.css?v=3" />
        <link rel="stylesheet" type="text/css" href="<?= URL::base(); ?>public/css/reviews.css?v=3" />
    </head>
    <body>
        <div class="fon">
            <div class="main">
                <div class="go-top">
                    <div class="text">Наверх</div>
                </div>
                <div id="block_callback" class="form-callback">
                    <div id="close-callback" class="close"></div>
                    <div class="title">Имя</div>
                    <input id="name" name="name" type="text" class="text" />
                    <div class="title">Телефон</div>
                    <input id="phone" name="phone" type="text" class="text"/>
                    <input class="ok" id="Ok" type="button" value="Отправить"/>
                </div>
                <div class="head ten-bottom">
                    <a href="<?= URL::base(); ?>"><div class="logo"></div></a>
                    <div class="menu-block">
                        <div class="menu">
                            <div class="elements">
                                <div id="order-elem-menu" class="elem"><a href="<?= URL::base(); ?>">ЗАКАЗАТЬ</a></div>	
                                <div id="video-elem-menu" class="elem"><a href="<?= URL::base(); ?>#video-block">КАК ЭТО РАБОТАЕТ</a></div>
                                <div id="product-elem-menu" class="elem"><a href="<?= URL::base(); ?>#product-block">ДЛЯ ЧЕГО?</a></div>
                                <div id="contact-elem-menu" class="elem"><a href="<?= URL::base(); ?>#contact-block">КОНТАКТЫ</a></div>
                                <div class="elem review cur"><a  class="cur"href="<?= URL::base(); ?>reviews.html">ОТЗЫВЫ</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="phone-block">
                        <div class="phone-number">8 953 588 71 23</div>
                    </div>
                </div>
                <div class="content-block">				
                    <div class="reviews">	
                        <div class="predhead">SHARE PLUMS</div><h1>ОТЗЫВЫ</h1>
                        <div class="list">
                            <div class="review">
                                <img class="photo" src="<?= URL::base(); ?>public/i/photo/1.png" />
                                <div class="name">АННА АНДРЕЕВА</div>
                                <div class="text">
                                    Полгода назад я решила заняться собой, и как раз, кстати, от подруги узнала про то, что в диете помогает слива Share Plums.
                                    Хотела сказать спасибо, моё похудение прошло вкусно и с пользой! Моя победа - 18 кг!
                                </div>
                            </div>					
                            <div class="review">
                                <img class="photo" src="<?= URL::base(); ?>public/i/photo/2.png" />
                                <div class="name">КОНСТАНТИН</div>
                                <div class="text">
                                    Рано или поздно наступает момент, когда понимаешь, что в организме накопилось много вредного. Конечно спорт и банька
                                    помогает, но особенно хорошо себя почувствовал после курса сливы Шер. Ел два месяца примерно.
                                </div>
                            </div>
                            <div class="review">
                                <img class="photo" src="<?= URL::base(); ?>public/i/photo/3.png" />
                                <div class="name">ВАСИЛИЙ</div>
                                <div class="text">
                                    Честно скажу не ожидал!!! Бывало после встречи с друзьями на утро очень плохо. Решил попробовать пробник сливы и 
                                    утром после посиделок было очень даже хорошо! Беру теперь в отпуск пачечку, на всякий случай:)) 
                                </div>
                            </div>
                        </div>
                    </div>			
                </div>
                <div class="footer">
                    <div class="text">SHARE PLUMS &copy; 2017</div>
                    <a href="https://vk.com/club115814247" target="_blank" ><div class="vk"></div></a>
                </div>
            </div>
        </div>
    </body>
    <script language="javascript" src="<?= URL::base(); ?>public/js/jquery-3.2.0.min.js"></script>
    <script language="javascript" src="<?= URL::base(); ?>public/js/jquery.scrollTo.min.js"></script>
    <script language="javascript" src="<?= URL::base(); ?>public/js/main.js?v=3"></script>
    <script language="javascript" src="<?= URL::base(); ?>public/js/menu.js?v=3"></script>
    <script language="javascript" src="<?= URL::base(); ?>public/js/plus-minus-count.js?v=3"></script>
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46626306 = new Ya.Metrika({ id:46626306, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/46626306" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</html>