<!DOCTYPE html>
<html lang="en">
    <head>
        <title>404 - Страница не найдена</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?= URL::base('http'); ?>public/i/ico.ico" />
        <link rel="stylesheet" type="text/css" href="<?= URL::base('http'); ?>public/css/style.css" />
    </head>
    <body>
        <div class="fon">
            <div class="main">
                <div class="go-top">
                    <div class="text">Наверх</div>
                </div>
                <div id="block_callback" class="form-callback">
                    <div id="close-callback" class="close"></div>
                    <div class="title">Имя</div>
                    <input id="name" name="name" type="text" class="text" />
                    <div class="title">Телефон</div>
                    <input id="phone" name="phone" type="text" class="text"/>
                    <input class="ok" id="Ok" type="button" value="Отправить"/>
                </div>
                <div class="head ten-bottom">
                    <a href="<?= URL::base(); ?>"><div class="logo"></div></a>
                    <div class="menu-block">
                        <div class="menu">
                            <div class="elements">
                                <div id="order-elem-menu" class="elem"><a href="<?= URL::base('http'); ?>">ЗАКАЗАТЬ</a></div>	
                                <div id="video-elem-menu" class="elem"><a href="<?= URL::base('http'); ?>#video-block">КАК ЭТО РАБОТАЕТ</a></div>
                                <div id="product-elem-menu" class="elem"><a href="<?= URL::base('http'); ?>#product-block">ДЛЯ ЧЕГО?</a></div>
                                <div id="contact-elem-menu" class="elem"><a href="<?= URL::base('http'); ?>#contact-block">КОНТАКТЫ</a></div>
                                <div id="review-elem-menu" class="elem review"><a href="<?= URL::base('http'); ?>reviews.html">ОТЗЫВЫ</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="phone-block">
                        <div class="phone-number"><?= $phone; ?></div>
                    </div>
                </div>
                <div class="content-block">
                    <div class="one-block">
                        <h2>Извините, такая страница не найдена.</h2>
                    </div>
                </div>
            </div>
        </div>
    </body>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/jquery-3.2.0.min.js"></script>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/jquery.scrollTo.min.js"></script>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/main.js"></script>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/menu.js"></script>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/plus-minus-count.js"></script>
</html>
