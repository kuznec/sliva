<div id="order-block" class="one-block">
    <div class="left">	
        <h1><?= $info->h1; ?></h1>
        <div class="sep-line"></div>
        <div class="text"><?= $info->text1; ?></div>
        <div class="form-order">
            <form method="POST" action="<?= URL::base('http') ?>send">
                <div class="text">Ваше имя</div>
                <input type="text" name="name" class="field"  />
                <div class="text">Номер телефона</div>
                <input type="text" name="phone" class="field" />
                <div class="block-count">
                    <div class="text-block">
                        <div class="top">Количество</div>
                        <div class="bottom">Цена 1 шт. - одна коробочка</div>
                    </div>
                    <div class="count-change-block">
                        <div id="minus-num-plums" class="left-button">-</div>
                        <input id="count-plums" type="text" name="count" class="count-packets" value="1"  maxlength="3"/>
                        <div id="plus-num-plums" class="right-button">+</div>
                    </div>
                </div>
                <div class="ok-block">
                    <div class="price">
                        <span id="all-price" class="all-price"><?= $info->cost1; ?></span> руб.
                    </div>
                    <input type="hidden" name="zakaz" value="1">
                    <input type="submit" class="ok-button" value="Заказать" />
                </div>
            </form>
        </div>
        <div class="give-me-my-share">
            <span>Получить <strong>бесплатный</strong> пробник</span>
        </div>
    </div>			
    <div class="right">
        <div class="head-text"><?= $info->h2; ?></div>
        <div class="list-advantages">
            <ul>
                <?= $info->text2; ?>
            </ul>
        </div>
        <div class="price-block">
            <h2><?= $info->h3; ?></h2>
            <ul><?= $info->text3; ?></ul>
        </div>
    </div>
</div>