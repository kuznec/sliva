<div class="two-block">
    <div id="video-block">
        <div class="left">
            <div class="video"><?= $info->text3; ?></div>
        </div>
        <div class="right">
            <div class="right-content">
                <h1><?= $info->h2; ?></h1>
                <div class="sep-line"></div>
                <div class="text"><?= $info->text2; ?></div>
                <div class="image-share"></div>
            </div>
        </div>
    </div>
</div>