<html>
    <head>
        <title><?= $title; ?></title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?= $description; ?>" />
        <meta name="keywords" content="<?= $keywords; ?>" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?= URL::base('http'); ?>public/i/ico.ico" />
        <link rel="stylesheet" type="text/css" href="<?= URL::base('http'); ?>public/css/style.css?v4" />
    </head>
    <body>
        <div class="fon">
            <div class="main">
                <div class="go-top">
                    <div class="text">Наверх</div>
                </div>
                <div class="say-ok novisible">Спасибо за обращение, мы Вам перезвоним!</div>
                <div class="callback-form novisible">
                    <div class="close-callback-form"></div>
                    <div class="top-line">Оставьте Ваш контакт.<br />Мы Вам перезвоним!</div>
                    <form method="POST" action="<?= URL::base('http') ?>send">
                        <div class="name-field">Ваше имя</div>
                        <input id="name-client" name="name" type="text" class="field" />
                        <div class="name-field">Ваш телефон</div>
                        <input id="phone-client" name="phone" type="text" class="field" />
                        <div class="bottom-line">
                            <input type="hidden" name="probs" value="1">
                            <input type="submit" id="ok-button" class="ok-button" value="Отправить" />
                        </div>
                    </form>
                </div>
                <div class="head ten-bottom">
                    <a href="<?= URL::base(); ?>"><div class="logo"></div></a>
                    <div class="menu-block">
                        <div class="menu">
                            <div class="elements">
                                <?= $menu; ?>
                            </div>
                        </div>
                    </div>
                    <div class="phone-block">
                        <div class="phone-number"><?= $phone; ?></div>
                    </div>
                </div>
                <div class="content-block">
                    <?= $content; ?>
                </div>
            </div>
        </div>
    </body>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/jquery-3.2.0.min.js"></script>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/jquery.scrollTo.min.js"></script>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/main.js?v3"></script>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/menu.js?v3"></script>
    <script language="javascript" src="<?= URL::base('http'); ?>public/js/plus-minus-count.js?v3"></script>
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46626306 = new Ya.Metrika({ id:46626306, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/46626306" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</html>