<script src="<?= URL::base(); ?>public/js/bootstrap.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
<script src="<?= URL::base(); ?>public/js/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?= URL::base(); ?>public/js/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?= URL::base(); ?>public/js/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<script src="<?= URL::base(); ?>public/js/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<article class="content item-editor-page">
    <div class="tab_container">
        <form name="item" method="POST">
            <div class="card card-block"><?php
                foreach($slide as $k => $v)
                { ?>
                    <div class="form-group row">
                        <div class="col-sm-12">
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">Время показа:</label>
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input data-inputmask="'alias': 'hh:ii:ss'" data-mask type="text" id="start" name="start" class="form-control boxed" value="<?= $v->start; ?>">
                                        </div>
                                    </div>
                                    <div class="col-sm-6">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                            </div>
                                            <input data-inputmask="'alias': 'hh:ii:ss'" data-mask type="text" id="stop" name="stop" class="form-control boxed" value="<?= $v->stop; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">h1:</label>
                                <input type="text" name="h1" class="form-control boxed" value="<?= $v->h1; ?>">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">text1:</label>
                                <textarea class="form-control boxed" id="text" name="text1"><?= $v->text1; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">Цена1:</label>
                                <input type="text" name="cost1" class="form-control boxed" value="<?= $v->cost1; ?>">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">h2:</label>
                                <input type="text" name="h2" class="form-control boxed" value="<?= $v->h2; ?>">
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">text2:</label>
                                <textarea class="form-control boxed" name="text2"><?= $v->text2; ?></textarea>
                            </div>
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">h3:</label>
                                <input type="text" name="h3" class="form-control boxed" value="<?= $v->h3; ?>">
                            </div>
                            
                            <div class="form-group">
                                <label class="form-control-label text-xs-right">text3:</label>
                                <textarea class="form-control boxed" name="text3"><?= $v->text3; ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-12">
                                <input type="hidden" name="id" value="<?= $v->id; ?>">
                                <button type="submit" class="btn btn-primary">Сохранить</button>
                            </div>
                        </div>
                    </div><?php
                } ?>
            </div>
        </form>
    </div>
</article>
<script src="https://cdn.ckeditor.com/4.5.7/standard/ckeditor.js"></script>
<script>
$(function () {
    CKEDITOR.replace('text');
    $("#start").inputmask("hh:mm:ss", {"placeholder": "hh:mm:ss"});
    $("#stop").inputmask("hh:mm:ss", {"placeholder": "hh:mm:ss"});
    $("[data-mask]").inputmask();
});
</script>