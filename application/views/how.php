<html>
    <head>
        <title>SHAREPLAMS.RU - Слива, улучшающая Ваш организм</title>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="shortcut icon" href="<?= URL::base(); ?>public/i/ico.ico" />
        <link rel="stylesheet" type="text/css" href="/public/css/style.css?v3" />
        <link rel="stylesheet" type="text/css" href="/public/css/works.css?v3" />
    </head>
    <body>
        <div class="fon">
            <div class="main">
                <div class="go-top">
                    <div class="text">Наверх</div>
                </div>
                <div class="say-ok novisible">Спасибо за обращение, мы Вам перезвоним!</div>
                <div class="callback-form novisible">
                    <div class="close-callback-form"></div>
                    <div class="top-line">Оставьте Ваш контакт.<br />Мы Вам перезвоним!</div>
                    <form method="POST" action="<?= URL::base('http') ?>send">
                        <div class="name-field">Ваше имя</div>
                        <input id="name-client" name="name" type="text" class="field" />
                        <div class="name-field">Ваш телефон</div>
                        <input id="phone-client" name="phone" type="text" class="field" />
                        <div class="bottom-line">
                            <input type="hidden" name="probs" value="1">
                            <input type="submit" id="ok-button" class="ok-button" value="Отправить" />
                        </div>
                    </form>
                </div>
                <div class="head ten-bottom">
                    <a href="<?= URL::base(); ?>"><div class="logo"></div></a>
                    <div class="menu-block">
                        <div class="menu">
                            <div class="elements">
                                <div id="order-elem-menu" class="elem"><a href="<?= URL::base('http'); ?>">ЗАКАЗАТЬ</a></div>	
                                <div id="video-elem-menu" class="elem"><a href="<?= URL::base('http'); ?>#video-block">КАК ЭТО РАБОТАЕТ</a></div>
                                <div id="product-elem-menu" class="elem"><a href="<?= URL::base('http'); ?>#product-block">ДЛЯ ЧЕГО?</a></div>
                                <div id="contact-elem-menu" class="elem"><a href="<?= URL::base('http'); ?>#contact-block">КОНТАКТЫ</a></div>
                                <div id="review-elem-menu" class="elem review"><a href="<?= URL::base('http'); ?>reviews.html">ОТЗЫВЫ</a></div>
                            </div>
                        </div>
                    </div>
                    <div class="phone-block">
                        <div class="phone-number">8 953 588 71 23</div>
                    </div>
                </div>
                <div class="content-block">
                    <div class="one-type-block">
                        <div class="description">	
                            <h1>О ПОЛЬЗЕ SHARE PLUMS</h1>
                            <div class="data">
                                <div class="predh">SHARE PLUMS</div>
                                <h2>ОЧИЩАЕТ ОРГАНИЗМ</h2>
                                <div class="sep-line-white"></div>
                                <div class="text">
                                    Благодаря самой сливе и порошку кассии, кишечник получает улучшение "проходных" функций. Также улучшается
                                    общая эффективность работы кишечника. А семена сливы в свою очередь нормализуют работу стула, благодаря чему
                                    значительно улучшается самочувствие.
                                </div>
                                <div class="give-me-my-share-yel">
                                    <span>Получить <strong>бесплатный</strong> пробник</span>
                                </div>
                            </div>
                        </div>			
                    </div>
                    <div class="two-type-block">
                        <div class="description">	
                            <div class="data">
                                <div class="predh">SHARE PLUMS</div>
                                <h2>ПРИ СБРОСЕ ВЕСА</h2>
                                <div class="sep-line"></div>
                                <div class="text">
                                    Так как правильное пищеварение является одной из основ правильного обмена веществ, а также усваивания
                                    питательных веществ, то благодаря правильной работе пищеварительной системы и в частности кишечника, люди,
                                    употребляющие сливу Share регулярно получают снижение в весе уже через полторы-две недели. А улучшение самочувствия 
                                    и лёгкость в организме уже через неделю ощущают более 60% любителей сливы и печенья компании «Belixian».
                                </div>
                                <div class="give-me-my-share">
                                    <span>Получить <strong>бесплатный</strong> пробник</span>
                                </div>
                            </div>
                        </div>			
                    </div>
                    <div class="three-type-block">
                        <div class="description">	
                            <div class="data">
                                <div class="predh">SHARE PLUMS</div>
                                <h2>ПРОТИВ ПОХМЕЛЬЯ</h2>
                                <div class="sep-line"></div>
                                <div class="text">
                                    Слива компании «Belixian» имеет ещё и эффект за счёт вышеупомянутого улучшения пищеварения и прохода кишечника, а также
                                    за счёт того, что порошке листьев шелковицы содержится много витамина C, аминокислот и множества антиоксидантов.
                                </div>
                                <div class="give-me-my-share">
                                    <span>Получить <strong>бесплатный</strong> пробник</span>
                                </div>
                            </div>
                        </div>			
                    </div>
                </div>
                <div class="footer">
                    <div class="text">SHARE PLUMS &copy; 2017</div>
                    <a href="https://vk.com/club115814247" target="_blank" ><div class="vk"></div></a>
                </div>
            </div>
        </div>
    </body>
    <script language="javascript" src="/public/js/jquery-3.2.0.min.js"></script>
    <script language="javascript" src="/public/js/jquery.scrollTo.min.js"></script>
    <script language="javascript" src="/public/js/main.js?v3"></script>
    <script language="javascript" src="/public/js/menu.js?v3"></script>
    <script language="javascript" src="/public/js/plus-minus-count.js?v3"></script>
    <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter46626306 = new Ya.Metrika({ id:46626306, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks"); </script> <noscript><div><img src="https://mc.yandex.ru/watch/46626306" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->
</html>