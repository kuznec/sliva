<div id="contact-block" class="contact-block">
    <div class="text-info">
        <h1>КОНТАКТЫ</h1>
        <div class="sep-line"></div>
        <div class="office">
            Офис продаж
        </div>
        <div class="text">
            <?= $address; ?>
        </div>
        <div class="email">
            <span><?= $email; ?></span>
        </div>
        <div class="phone">
            <?= $phone; ?>
        </div>
    </div>
    <div class="map-info">
        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Adedf8f72f186cdcedd2765023cd070f35bc9f697fb533717db306bd143f63d68&amp;width=450&amp;height=250&amp;lang=ru_RU&amp;scroll=true"></script>
    </div>
</div>